# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME:** RUSLAN BAKHTIYAROV

**E-MAIL:** rusya.vay@mail.ru

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10 x64

# HARDWARE

- CPU: Intel® Core™ i5-8265U
- RAM: 8GB

# PROGRAM BUILD

```bash
mvn clean install
```
# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```
# SCREENSHOTS

https://yadi.sk/d/Yzh4rmppoi2fRw?w=1