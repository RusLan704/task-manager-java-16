package ru.bakhtiyarov.tm.exception.empty;

import ru.bakhtiyarov.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }

}
