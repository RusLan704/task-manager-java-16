package ru.bakhtiyarov.tm.exception.empty;

import ru.bakhtiyarov.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error! Role is empty...");
    }
    
}