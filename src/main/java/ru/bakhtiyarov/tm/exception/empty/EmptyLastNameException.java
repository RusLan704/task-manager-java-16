package ru.bakhtiyarov.tm.exception.empty;

import ru.bakhtiyarov.tm.exception.AbstractException;

public class EmptyLastNameException extends AbstractException {

    public EmptyLastNameException() {
        super("Error! Last name is empty...");
    }

}