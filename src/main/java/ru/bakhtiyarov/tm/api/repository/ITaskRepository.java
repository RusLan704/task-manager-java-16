package ru.bakhtiyarov.tm.api.repository;

import ru.bakhtiyarov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void add(String userId, Task project);

    void remove(String userId, Task project);

    List<Task> findAll(String userId);

    void clear(String userId);

    Task findOneById(String userId, String id);

    Task findOneByIndex(String userId, Integer index);

    Task findOneByName(String userId, String name);

    Task removeOneByIndex(String userId, Integer index);

    Task removeOneById(String userId, String id);

    Task removeOneByName(String userId, String name);

}
