package ru.bakhtiyarov.tm.api.service;

import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String id);

    User findByLogin(String login);

    User removeByLogin(String login);

    User removeById(String id);

    User updatePassword(String userId, String password);

    User updateUserEmail(String userId, String email);

    User updateUserFirstName(String userId, String lastName);

    User updateUserLastName(String userId, String LastName);

    User updateUserMiddleName(String userId, String middleName);

    User updateUserLogin(String userId, String login);

    User lockUserByLogin(String login);

    User unLockUserByLogin(String login);

}
