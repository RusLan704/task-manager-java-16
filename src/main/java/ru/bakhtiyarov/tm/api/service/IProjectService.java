package ru.bakhtiyarov.tm.api.service;

import ru.bakhtiyarov.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, Project task);

    void remove(String userId, Project task);

    List<Project> findAll(String userId);

    void clear(String userId);

    Project findOneById(String userId, String id);

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project removeOneByIndex(String userId, Integer index);

    Project removeOneById(String userId, String id);

    Project removeOneByName(String userId, String name);

    Project updateProjectById(String userId, String id, String name, String description);

    Project updateProjectByIndex(String userId, Integer index, String name, String description);

}
