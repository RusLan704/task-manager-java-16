package ru.bakhtiyarov.tm.service;

import ru.bakhtiyarov.tm.api.repository.IUserRepository;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.empty.*;
import ru.bakhtiyarov.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setLogin(login);
        user.setRole(Role.USER);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public User updatePassword(final String userId, final String password) {
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(userId);
        if (user == null) return null;
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUserEmail(final String userId, final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = findById(userId);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User updateUserFirstName(final String userId, final String firstName) {
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        final User user = findById(userId);
        if (user == null) return null;
        user.setFistName(firstName);
        return user;
    }

    @Override
    public User updateUserLastName(final String userId, final String lastName) {
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        final User user = findById(userId);
        if (user == null) return null;
        user.setLastName(lastName);
        return user;
    }

    @Override
    public User updateUserMiddleName(final String userId, final String middleName) {
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        final User user = findById(userId);
        if (user == null) return null;
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User updateUserLogin(final String userId, final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findById(userId);
        if (user == null) return null;
        user.setLogin(login);
        return user;
    }

    @Override
    public User lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(true);
        return user;
    }

    @Override
    public User unLockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        return user;
    }

}