package ru.bakhtiyarov.tm.command.project;

import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.PROJECT_REMOVE_BY_INDEX;
    }

    @Override
    public String description() {
        return "Remove project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().removeOneByIndex(userId, index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
