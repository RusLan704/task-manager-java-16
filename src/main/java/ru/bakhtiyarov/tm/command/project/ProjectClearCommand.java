package ru.bakhtiyarov.tm.command.project;

import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;

public class ProjectClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.PROJECT_CLEAR;
    }

    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECT]");
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectService().clear(userId);
        System.out.println("[OK]");
    }

}
