package ru.bakhtiyarov.tm.command.project;

import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.entity.Project;

import java.util.List;

public class ProjectListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.PROJECT_LIST;
    }

    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        final String userId = serviceLocator.getAuthService().getUserId();
        final List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        for (final Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

}