package ru.bakhtiyarov.tm.command.system;

import ru.bakhtiyarov.tm.command.AbstractCommand;

import java.util.List;

public class ArgumentsCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-arg";
    }

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (AbstractCommand command : commands) {
            final String arg = command.arg();
            if (arg != null)
                System.out.println(command.arg());
        }
    }

}
